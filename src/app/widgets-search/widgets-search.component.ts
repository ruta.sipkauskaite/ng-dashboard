import { Component, ChangeDetectorRef } from '@angular/core';

import { ResourcesService } from '../resources/widget/resources.service';
import { debounceTime, distinctUntilChanged, switchMap, skip, filter, tap } from 'rxjs/operators';
import { SearchService } from '../resources/search.service';

@Component({
  selector: 'app-widgets-search',
  templateUrl: './widgets-search.component.html',
  styleUrls: ['./widgets-search.component.css']
})

export class WidgetsSearchComponent {

  readonly widgets$ = this.searchService.value$.pipe(
    skip(1),
    debounceTime(500),
    distinctUntilChanged(),
    filter(x => !!x),
    switchMap(query => this.resourcesService.fetchhWidgets(query)),
  );

  constructor(
    private resourcesService: ResourcesService,
    private searchService: SearchService,
  ) {}
}
