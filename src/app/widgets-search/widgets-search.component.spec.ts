import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetsSearchComponent } from './widgets-search.component';

describe('WidgetsSearchComponent', () => {
  let component: WidgetsSearchComponent;
  let fixture: ComponentFixture<WidgetsSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetsSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetsSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
