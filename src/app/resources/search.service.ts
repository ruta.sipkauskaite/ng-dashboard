import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
    private readonly searchValueSubject = new BehaviorSubject<string>('');
    
    get value() {
        return this.searchValueSubject.getValue();
    }

    set value(query: string) {
        this.searchValueSubject.next(query);
    }

    get value$() {
        return this.searchValueSubject.asObservable();
    }
}