import { Component, OnInit, Input } from '@angular/core';
import { ResourcesService } from '../resources/widget/resources.service';

import { Widget } from '../resources/widget//models';
import { SearchService } from '../resources/search.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  widgets: Widget[];
  searchValue$ = this.searchService.value$;

  constructor(
    private resourcesService: ResourcesService,
    private searchService: SearchService,
    ) { }

  ngOnInit() {
    this.resourcesService.getWidgets().subscribe(
      response => this.widgets = response
    );
  }

  onWidgetDelete(id:number) {
    this.resourcesService.deleteWidget(id).subscribe(
      response => this.widgets = this.widgets.filter(widget => widget.id !== id)
    );
  }



}
