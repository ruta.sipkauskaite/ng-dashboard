import { Component, Output, EventEmitter, Input } from '@angular/core';
import { SearchService } from '../resources/search.service';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent {
  @Output() toggleSidebar = new EventEmitter();

  constructor(private searchService: SearchService) { }

  public searchRequest(query: string): void {
    this.searchService.value = query;
  }
}
